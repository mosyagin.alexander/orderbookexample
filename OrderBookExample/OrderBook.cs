using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace OrderBookExample
{
    public enum Side
    {
        Bid,
        Ask
    }

    public interface IOrderBook
    {
        // Обновить один уровень ордербука (объем заявок по заданной цене)
        public void Update(Side side, decimal price, decimal size, bool ignoreError = false);

        // заполнить одну сторону ордербука новыми данными
        public void Fill(Side side, IEnumerable<Tuple<decimal, decimal>> data);

        // очистить ордербук. возвращает количество удаленных уровней для Bid и Ask
        public Tuple<int, int> Clear();

        // получить верхний уровень ордербука -- лучшую цену и объем для бидов и асков
        public BidAsk GetBidAsk();

        // получить count верхних уровней одной стороны ордербука.
        // cumulative -- считать кумулятивные объемы
        public Level[] GetTop(Side side, int count, bool cumulative = false);

        // получить несколько верхних уровней ордербука, вплоть до цены price включительно
        // cumulative -- считать кумулятивные объемы
        public Level[] GetTop(Side side, decimal price, bool cumulative = false);

        // получить цену с уровня, где кумулятивный объем превышает cumul
        public decimal? GetPriceWhenCumulGreater(Side side, decimal cumul);

        // возвращает true, если ордербук пуст
        public bool IsEmpty();

    }

    public class OrderBookBase
    {
        protected decimal priceMultiplier;
        protected decimal sizeMultiplier;

        protected OrderBookBase() { }

        // pricePrecision -- сколько цифр после запятой в ценах
        // sizePrecision -- сколько цифр после запятой в объемах
        public OrderBookBase(uint pricePrecision, uint sizePrecision)
        {
            priceMultiplier = (decimal)Math.Pow(10, -pricePrecision);
            sizeMultiplier = (decimal)Math.Pow(10, -sizePrecision);
        }
    }
    class CumulComparer : IComparer<Level>
    {
        public int Compare(Level x, Level y)
            => (x.CumulSize ?? 0).CompareTo(y.CumulSize ?? 0);
    }
    class OrderComparer : IEqualityComparer<Level>, IComparer<Level>
    {
        private readonly bool Reverse;

        public OrderComparer(bool reverse = false)
            => this.Reverse = reverse;

        public int Compare(Level x, Level y)
            => this.Reverse ? y.Price.CompareTo(x.Price) : x.Price.CompareTo(y.Price);

        public bool Equals(Level x, Level y)
            => decimal.Equals(x.Price, y.Price);

        public int GetHashCode([DisallowNull] Level obj)
            => obj.Price.GetHashCode();
    }
    public class OrderBook : OrderBookBase, IOrderBook
    {
        private readonly OrderComparer Comparer = new(); // прямое сравнение - для Asks
        private readonly OrderComparer ReverseComparer = new(true); // обратное  сравнение - для Bids
        Level[] Asks = Array.Empty<Level>();
        Level[] Bids = Array.Empty<Level>();

        private decimal RoundPrice(decimal a)
            => decimal.Round(a / priceMultiplier) * priceMultiplier;
        private decimal RoundSize(decimal a)
            => decimal.Round(a / sizeMultiplier) * sizeMultiplier;

        public OrderBook(uint pricePrecision, uint sizePrecision) : base(pricePrecision, sizePrecision) { }
        public Tuple<int, int> Clear()
        {
            int a = Asks.Length;
            int b = Bids.Length;
            Asks = Array.Empty<Level>();
            Bids = Array.Empty<Level>();
            return new Tuple<int, int>(b, a);
        }

        public void Fill(Side side, IEnumerable<Tuple<decimal, decimal>> data)
        {
            decimal cumulative = 0;
            Level[] prepared = data
                .Select(t => new Level(RoundPrice(t.Item1), RoundSize(t.Item2)))
                .Distinct(Comparer) // price должно быть уникальным, надо проверять и обрабатывать, но не будем усложнять
                .ToArray();
            // Asks и Bids храним в отсортированном виде для ускорения работы
            if (side == Side.Ask)
            {
                Array.Sort(prepared, Comparer);
                Asks = prepared.Select(l => new Level(l.Price, l.Size, cumulative += l.Size)).ToArray();
            }
            else
            {
                Array.Sort(prepared, ReverseComparer);
                Bids = prepared.Select(l => new Level(l.Price, l.Size, cumulative += l.Size)).ToArray();
            }
        }

        public BidAsk GetBidAsk()
            => new() { AskPrice = Asks[0].Price, AskVolume = Asks[0].Size, BidPrice = Bids[0].Price, BidVolume = Bids[0].Size };

        public decimal? GetPriceWhenCumulGreater(Side side, decimal cumul)
        {
            if (side == Side.Ask)
            {
                int p = Array.BinarySearch(Asks, new Level(0, 0, cumul), new CumulComparer());
                if (p < Asks.Length && p > ~Asks.Length)
                    return p >= 0 ? Asks[p].Price : Asks[~p].Price;
            }
            else
            {
                int p = Array.BinarySearch(Bids, new Level(0, 0, cumul), new CumulComparer());
                if (p < Bids.Length && p > ~Bids.Length)
                    return p >= 0 ? Bids[p].Price : Bids[~p].Price;
            }
            return null;
        }

        public Level[] GetTop(Side side, int count, bool cumulative = false)
        {
            if (side == Side.Ask)
            {
                int length = Math.Min(count, Asks.Length); // скорее всего, если count > Asks.Count, то следует выбросить исключение
                // здесь узкое место, можно ускорить примерно в 10 раз, если сделать Level классом, а не структурой
                // можно и так Asks[0..length] и со Span и т.п., но принципиального ускорения не наблюдается
                Level[] result = new Level[length];
                Array.Copy(Asks, 0, result, 0, length);
                return result;
            }
            else
            {
                int length = Math.Min(count, Bids.Length); // скорее всего, если count > Bids.Count, то следует выбросить исключение
                Level[] result = new Level[length];
                Array.Copy(Bids, 0, result, 0, length);
                return result;
            }
        }

        public Level[] GetTop(Side side, decimal price, bool cumulative = false)
        {
            if (side == Side.Ask)
            {
                // сначала находим размер массива, а затем вызываем GetTop(Side, int, bool)
                int count = Array.BinarySearch(Asks, new Level(RoundPrice(price), 0m), Comparer);
                if (count >= 0) return GetTop(side, count + 1, cumulative);
                else return GetTop(side, 1 + ~count, cumulative);
            }
            else
            {
                int count = Array.BinarySearch(Bids, new Level(RoundPrice(price), 0m), ReverseComparer);
                if (count >= 0) return GetTop(side, count + 1, cumulative);
                else return GetTop(side, 1 + ~count, cumulative);
            }
        }

        public bool IsEmpty()
            => 0 == (Asks.Length + Bids.Length);

        public void Update(Side side, decimal price, decimal size, bool ignoreError = false)
        {
            // ignoreError - предполагаю, относится к ситуации когда нечего обновлять
            if (side == Side.Ask)
            {
                int pos = Array.BinarySearch(Asks, new Level(RoundPrice(price), 0m), Comparer);
                if (pos >= 0)
                {
                    decimal diff = size - Asks[pos].Size;
                    decimal c = Asks[pos].CumulSize ?? 0;
                    Asks[pos] = new Level(price, size, c + diff);
                    // параллельно работает немного быстрее
                    Parallel.For(pos + 1, Asks.Length, i => Asks[i] = new Level(Asks[i].Price, Asks[i].Size, diff + Asks[i].CumulSize ?? 0));
                    /*for (int i = pos + 1; i < Asks.Length; i++)
                    {
                        Asks[i] = new Level(Asks[i].Price, Asks[i].Size, diff + Asks[i].CumulSize ?? 0);
                    }*/
                }
                else
                {
                    if (!ignoreError) throw new KeyNotFoundException();
                }
            }
            else
            {
                int pos = Array.BinarySearch(Bids, new Level(RoundPrice(price), 0m), ReverseComparer);
                if (pos >= 0)
                {
                    decimal diff = size - Bids[pos].Size;
                    decimal c = Bids[pos].CumulSize ?? 0;
                    Bids[pos] = new Level(price, size, c + diff);
                    // параллельно работает немного быстрее
                    Parallel.For(pos + 1, Bids.Length, i => Bids[i] = new Level(Bids[i].Price, Bids[i].Size, diff + Bids[i].CumulSize ?? 0) );
                    /*for (int i = pos + 1; i < Bids.Length; i++)
                    {
                        Bids[i] = new Level(Bids[i].Price, Bids[i].Size, diff + Bids[i].CumulSize ?? 0);
                    }*/
                }
                else
                {
                    if (!ignoreError) throw new KeyNotFoundException();
                }
            }
        }
    }
}
