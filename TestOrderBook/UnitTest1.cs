using NUnit.Framework;
using OrderBookExample;
using System;
using System.Collections.Generic;

namespace TestOrderBook
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            OrderBook orderBook = new(3,3);
            Assert.IsTrue(orderBook.IsEmpty());
            orderBook.Fill(Side.Bid, new Tuple<decimal, decimal>[]
            {
                new (1.2m, 0.58m),
                new (1.56m, 0.52m),
                new (1.86m, 0.42m),
                new (1.95m, 0.12m),
                new (2.1m, 0.32m), 
            });
            orderBook.Fill(Side.Ask, new Tuple<decimal, decimal>[]
            {
                new (2.51m, 0.66m),
                new (2.81m, 0.66m),
                new (2.98m, 0.76m),
                new (3.2m, 0.96m),
                new (3.32m, 1.2m),
            });
            Assert.IsFalse(orderBook.IsEmpty());
            BidAsk ba = orderBook.GetBidAsk();
            Assert.AreEqual(2.51M, ba.AskPrice);
            Assert.AreEqual(2.1M, ba.BidPrice);

            Level[] levels = orderBook.GetTop(Side.Ask, 3, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.51m, levels[0].Price);
            Assert.AreEqual(2.81m, levels[1].Price);
            Assert.AreEqual(2.98m, levels[2].Price);
            Assert.AreEqual(0.66m, levels[0].Size);
            Assert.AreEqual(0.66m, levels[1].Size);
            Assert.AreEqual(0.76m, levels[2].Size);
            Assert.AreEqual(0.66m, levels[0].CumulSize);
            Assert.AreEqual(1.32m, levels[1].CumulSize);
            Assert.AreEqual(2.08m, levels[2].CumulSize);

            levels = orderBook.GetTop(Side.Ask, 2.97m, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.51m, levels[0].Price);
            Assert.AreEqual(2.81m, levels[1].Price);
            Assert.AreEqual(2.98m, levels[2].Price);
            Assert.AreEqual(0.66m, levels[0].Size);
            Assert.AreEqual(0.66m, levels[1].Size);
            Assert.AreEqual(0.76m, levels[2].Size);
            Assert.AreEqual(0.66m, levels[0].CumulSize);
            Assert.AreEqual(1.32m, levels[1].CumulSize);
            Assert.AreEqual(2.08m, levels[2].CumulSize);

            levels = orderBook.GetTop(Side.Ask, 2.98m, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.51m, levels[0].Price);
            Assert.AreEqual(2.81m, levels[1].Price);
            Assert.AreEqual(2.98m, levels[2].Price);
            Assert.AreEqual(0.66m, levels[0].Size);
            Assert.AreEqual(0.66m, levels[1].Size);
            Assert.AreEqual(0.76m, levels[2].Size);
            Assert.AreEqual(0.66m, levels[0].CumulSize);
            Assert.AreEqual(1.32m, levels[1].CumulSize);
            Assert.AreEqual(2.08m, levels[2].CumulSize);

            levels = orderBook.GetTop(Side.Bid, 3, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.1m, levels[0].Price);
            Assert.AreEqual(1.95m, levels[1].Price);
            Assert.AreEqual(1.86m, levels[2].Price);
            Assert.AreEqual(0.32m, levels[0].Size);
            Assert.AreEqual(0.12m, levels[1].Size);
            Assert.AreEqual(0.42m, levels[2].Size);
            Assert.AreEqual(0.32m, levels[0].CumulSize);
            Assert.AreEqual(0.44m, levels[1].CumulSize);
            Assert.AreEqual(0.86m, levels[2].CumulSize);

            levels = orderBook.GetTop(Side.Bid, 1.87m, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.1m, levels[0].Price);
            Assert.AreEqual(1.95m, levels[1].Price);
            Assert.AreEqual(1.86m, levels[2].Price);
            Assert.AreEqual(0.32m, levels[0].Size);
            Assert.AreEqual(0.12m, levels[1].Size);
            Assert.AreEqual(0.42m, levels[2].Size);
            Assert.AreEqual(0.32m, levels[0].CumulSize);
            Assert.AreEqual(0.44m, levels[1].CumulSize);
            Assert.AreEqual(0.86m, levels[2].CumulSize);

            levels = orderBook.GetTop(Side.Bid, 1.86m, true);
            Assert.AreEqual(3, levels.Length);
            Assert.AreEqual(2.1m, levels[0].Price);
            Assert.AreEqual(1.95m, levels[1].Price);
            Assert.AreEqual(1.86m, levels[2].Price);
            Assert.AreEqual(0.32m, levels[0].Size);
            Assert.AreEqual(0.12m, levels[1].Size);
            Assert.AreEqual(0.42m, levels[2].Size);
            Assert.AreEqual(0.32m, levels[0].CumulSize);
            Assert.AreEqual(0.44m, levels[1].CumulSize);
            Assert.AreEqual(0.86m, levels[2].CumulSize);

            decimal? price = orderBook.GetPriceWhenCumulGreater(Side.Ask, 1.31m);
            Assert.AreEqual(2.81m, price);
            price = orderBook.GetPriceWhenCumulGreater(Side.Bid, 0.85m);
            Assert.AreEqual(1.86m, price);
            price = orderBook.GetPriceWhenCumulGreater(Side.Ask, 100m);
            Assert.IsNull(price);
            price = orderBook.GetPriceWhenCumulGreater(Side.Bid, 100m);
            Assert.IsNull(price);

            orderBook.Update(Side.Ask, 2.51m, 0.33m);
            Assert.AreEqual(0.33m, orderBook.GetTop(Side.Ask, 2)[0].Size);
            Assert.AreEqual(0.33m, orderBook.GetTop(Side.Ask, 2)[0].CumulSize);
            Assert.AreEqual(0.99m, orderBook.GetTop(Side.Ask, 2)[1].CumulSize);
            orderBook.Update(Side.Bid, 1.95m, 1.33m);
            Assert.AreEqual(1.33m, orderBook.GetTop(Side.Bid, 3)[1].Size);
            Assert.AreEqual(1.65m, orderBook.GetTop(Side.Bid, 3)[1].CumulSize);
            Assert.AreEqual(2.07m, orderBook.GetTop(Side.Bid, 3)[2].CumulSize);

            orderBook.Update(Side.Ask, 2.50m, 0.33m, true);
            Assert.Throws<KeyNotFoundException>(() => orderBook.Update(Side.Ask, 2.50m, 0.33m, false));
            orderBook.Update(Side.Bid, 1.94m, 1.33m, true);
            Assert.Throws<KeyNotFoundException>(() => orderBook.Update(Side.Bid, 1.94m, 1.33m, false));

            orderBook.Clear();
            Assert.IsTrue(orderBook.IsEmpty());
        }
    }
}