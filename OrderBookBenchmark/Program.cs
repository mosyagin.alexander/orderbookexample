﻿using OrderBookExample;

const int CycleCount = 10000;
const int OrderBookSize = 10000;
const int HalfCycleCount = CycleCount/2;

Random rnd = new Random();

OrderBook orderBook = new(5,5);

orderBook.Fill(
    Side.Ask,
    Enumerable
        .Repeat(1, OrderBookSize)
        .Select(v => new Tuple<decimal, decimal>((decimal)(10 + rnd.NextDouble() * 5), (decimal)(rnd.NextDouble()))));
orderBook.Fill(
    Side.Bid,
    Enumerable
        .Repeat(1, OrderBookSize)
        .Select(v => new Tuple<decimal, decimal>((decimal)(5 + rnd.NextDouble() * 5), (decimal)(rnd.NextDouble()))));

DateTime start, stop;
again:
// GetBidAsk

start = DateTime.Now;
for (int i = 0; i < CycleCount; i++)
{
    orderBook.GetBidAsk();
}
stop = DateTime.Now;
Console.WriteLine($"GetBidAsk: {stop - start}");

// Update

start = DateTime.Now;
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.Update(Side.Ask, (decimal)(10 + rnd.NextDouble() * 5), (decimal)(rnd.NextDouble()), true);
}
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.Update(Side.Bid, (decimal)(5 + rnd.NextDouble() * 5), (decimal)(rnd.NextDouble()), true);
}
stop = DateTime.Now;
Console.WriteLine($"Update: {stop - start}");

// GetTop`2

start = DateTime.Now;
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetTop(Side.Ask, OrderBookSize / 2, true);
}
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetTop(Side.Bid, OrderBookSize / 2, true);
}
stop = DateTime.Now;
Console.WriteLine($"GetTop`2: {stop - start}");

// GetTop`1

start = DateTime.Now;
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetTop(Side.Ask, 12.25m, true);
}
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetTop(Side.Bid, 7.25m, true);
}
stop = DateTime.Now;
Console.WriteLine($"GetTop`1: {stop-start}");

// GetPriceWhenCumulGreater

start = DateTime.Now;
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetPriceWhenCumulGreater(Side.Ask, OrderBookSize / 2);
}
for (int i = 0; i < HalfCycleCount; i++)
{
    orderBook.GetPriceWhenCumulGreater(Side.Bid, OrderBookSize / 2);
}
stop = DateTime.Now;
Console.WriteLine($"GetPriceWhenCumulGreater: {stop - start}");
goto again;